#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
import sys
from datetime import datetime

if len(sys.argv) != 5 or "-h" in sys.argv:
  print("Usage: {} [VJ/KB] DD/MM/YY start_hour end_hour".format(sys.argv[0]))
  sys.exit(1)

ids = {"kb": [72, 969], "vj": [121]}

start = int(datetime.strptime(sys.argv[2] + " " + sys.argv[3], "%d/%m/%y %H").timestamp())
end = int(datetime.strptime(sys.argv[2] + " " + sys.argv[4], "%d/%m/%y %H").timestamp())

res = requests.get("http://v2.webservices.chronos.epita.net/api/v2/Room/GetFree/{}/{}".format(start, end))

if res.status_code != 200:
  print("Oops, something went wrong.")
  sys.exit(1)

res = res.json()

for room in res:
  if int(room["ParentId"]) in ids[sys.argv[1].lower()]:
    print(room["Name"])
