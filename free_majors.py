#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
import sys
from datetime import datetime

if len(sys.argv) != 4 or "-h" in sys.argv:
  print("Usage: {} DD/MM/YY start_hour end_hour".format(sys.argv[0]))
  sys.exit(1)

majors = {"CSI":17, "GISTRE":18, "GITM":906, "MTI":19, "SCIA":20, "SIGL":21, "SRS":22, "TCOM":23}

start = datetime.strptime(sys.argv[1] + " " + sys.argv[2], "%d/%m/%y %H")
end = datetime.strptime(sys.argv[1] + " " + sys.argv[3], "%d/%m/%y %H")
week = (start - datetime(2014, 9, 1)).days // 7 + 1 # Thank you chronos...

occupation = {major: None for major in majors}

for major, id in majors.items():
    if occupation[major] is not None:
        continue # common course

    res = requests.get("http://v2.webservices.chronos.epita.net/api/v2/Week/GetMyWeek/{}/{}/1".format(week, id))

    if res.status_code != 200:
        print("Oops, something went wrong.")
        sys.exit(1)

    for day in res.json()["DayList"]:
        for course in day["CourseList"]:
            s = datetime.strptime(course["BeginDate"], "%Y-%m-%dT%H:%M:%SZ")
            e = datetime.strptime(course["EndDate"], "%Y-%m-%dT%H:%M:%SZ")
            if s.day != start.day:
                break # wrong day
            if not (end <= s or start >= e):
                for m in course["GroupList"]:
                    if m["Name"] in majors:
                        occupation[m["Name"]] = course["Name"]

for m, c in occupation.items():
    if c is not None:
        print("\033[1;31m"  + m + "\033[0m:", c)
    else:
        print("\033[1;32m"  + m + "\033[0m:", "free")
